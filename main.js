const file = require('file');
const cron = require('cron');
const request = require('request');

const meterId = 2;
let reading = 1000;
const url = `http://localhost:8080/api/bills/${meterId}`;

const triggerCall = () => {
    const currentReading = getReading();
    return new Promise((resolve, reject) => {
        request(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic dW1hOndlbGNvbWUxMjM='
            },
            body: { meterId, currentReading },
            json: true,
        }, function (error, response, body) {
          console.error('error:', error); // Print the error if one occurred
          console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
          console.log('body:', body); // Print the HTML for the Google homepage.
          resolve(response.statusCode)
        });
    });
}

const getReading = () => {
    const randomValue = Math.floor(Math.random() * 1000);
    const currentReading = reading + randomValue
    reading = currentReading;
    return currentReading;
}


async function main() {

    var CronJob = cron.CronJob;

    //cron-job for every month : 0 0 1 * *
    var job = new CronJob(
        '*/10 * * * * *',
        function () {
            console.log('Sending the reading to server..');
            triggerCall();
        },
    );
    job.start();
    //triggerCall();
}

main();

